# kangle

#### 开源版本的kangle服务器

> * 只是修改了错误页面的版权链接,在`/src/HttpCore.cpp`文件里面
> * 管理页面的版权在`/src/KHttpManage.cpp`文件里面
> * 管理统计在`/src/KHttpManage.cpp`文件里面
> * 全局常量在`/src/global.h`文件里面,可修改Response头信息,如Server: kangle/3.5.13.2(实际在`/src/KUploadProgressMark.h`文件里面)


#### 安装教程

1. clone源码执行`yum -y install wget make automake gcc gcc-c++ pcre-devel zlib-devel sqlite-devel openssl-devel libaio-devel`
2. 进入文件夹`./configure --prefix=/vhs/kangle --enable-disk-cache --enable-ipv6 --enable-ssl --enable-vh-limit`
3. 继续`make && make install`

#### 使用说明

1. 启动`/vhs/kangle/bin/kangle`
2. 进入`http://ip:3311`
3. 账号`admin`密码`kangle`

>安装`php` [链接](https://blog.csdn.net/hd2killers/article/details/80564783)

> 其他
> 添加kangle的请求规则,文件在`/vhs/kangle/etc/config.xml`,在`<request action='vhs' >`增加表BEGIN2

```xml
<table name='BEGIN2'>
	<chain  action='deny'  name='k_webshell'>
		<mark_post_file   filename='\.(php|phps|php5|php7|jsp|jspx|asp|aspx|asa|asax|ascx|ashx|asmx|axd|html|htm|js)$' icase='1'></mark_post_file>
	</chain>
	<chain  action='deny'  name='k_asp_loophole'>
		<acl_reg_path  nc='1' path='\.(asp|aspx|asa|asax|ascx|ashx|asmx|axd)[;/]'></acl_reg_path>
	</chain>
	<chain  action='deny'  name='k_htaccess'>
		<acl_url  nc='1'><![CDATA[\.(htaccess|uini)]]></acl_url>
	</chain>
	<chain  action='deny'  name='k_file'>
		<acl_url  nc='1'><![CDATA[(web\.config|httpd\.ini)]]></acl_url>
	</chain>
	<chain  action='continue'  name='k_realip'>
		<mark_replace_ip   header='X-Forwarded-For' > </mark_replace_ip>
	</chain>
	<chain  action='continue' >
		<acl_host  split='|'></acl_host>
	</chain>
</table>
```

> 添加之后,在BEGIN里面引用BEGIN2,即可实现规则

#### 使用技巧

> 防盗链
>> 访问`wz.lxx123.club`在`/web/resource/`目录里面进行302重定向`http://wz.lxx123.club/web/index.php`,
>> `Referer`是`header`模块里面的请求头属性
![截图](./md/img/N3]AT_}IW$~O[$8E][YKGOX.png)

> 禁止访问使用`cookie`检测
>> `Cookie`是`header`模块使用regex,取反检测存在`cookie`的`user_id=`,不存在禁止访问
![截图](./md/img/NB[$1QTEVFQQI@GL`T79A3X.png)

> 反向代理
>>  匹配和标记模块都使用`host`,由80转到8080端口
![截图](./md/img/U1BRL{W2$7NTWW3VYHGA638.png)

> 配置伪静态(tp框架,解决静态资源和隐藏index.php)
>> 匹配`host`和标记模块都`rewrite`
>> `prefix:/`
>> `path:(.*)`
>> `rewrite to:/index.php/$1`
>> 第二个标记模块都`rewrite`选择`OR NEXT`
>> `prefix:/`
>> `path:/static/`
>> `rewrite to:/`
![截图](./md/img/00005.png)