# docker build -t kangle:ubuntu20-bin .
FROM daocloud.io/library/ubuntu:20.04

# 替换为阿里源
# RUN sed -i 's/archive.ubuntu.com/mirrors.aliyun.com/g' /etc/apt/sources.list
COPY ./kangle_ub20.tar /opt
RUN cd /opt && tar xvf kangle_ub20.tar
ENV LD_LIBRARY_PATH=/opt/lib/
# 端口
EXPOSE 80 3311
# 指定容器启动时执行的命令
CMD ["/opt/release/kanglebin", "-d", "3"]
