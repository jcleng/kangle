-- 环境 daocloud.io/library/ubuntu:20.04
-- xmake依赖
-- apt install g++ unzip gcc git make libreadline-dev ccache
-- 使用 ./configure 生成 src/config.h
-- xmake编译之后下面的配置文件放到可执行文件的父目录
-- ./webadmin/
-- ./etc/
-- file build/linux/x86_64/release/kanglebin
-- build/linux/x86_64/release/kanglebin: ELF 64-bit LSB shared object, x86-64, version 1 (SYSV), dynamically linked, interpreter /lib64/ld-linux-x86-64.so.2, BuildID[sha1]=bae4094d9a9ecbd7da2451f27dc83dac34b26a54, for GNU/Linux 3.2.0, not stripped

-- 安装依赖
-- xmake f -cvD
-- 编译
-- xmake

-- 手动下载 build-artifacts
-- git clone --depth=1 https://gitee.com/xmake-mirror/build-artifacts.git /home/vagrant/.xmake/repositories/build-artifacts
-- git clone --depth=1 https://gitee.com/tboox/xmake-repo.git /home/vagrant/.xmake/repositories/xmake-repo

-- 项目依赖
add_requires("libaio 0.3.113")
add_requires("pcre 8.45")

target("kanglebin")  -- 设置目标程序名字
    set_kind("binary")   -- 设置编译成二进制程序，不设置默认编译成二进制程序，可选择编译成动静态库等
    -- set_kind("static")
    -- 设置使用的交叉编译工具链
    -- set_toolchains("myaarch64")
    -- 设置平台
    -- set_plat("cross")
    -- 设置架构
    -- set_arch("aarch64")

-- 添加依赖对应的包, 对应的so在~/.xmake里面,执行 LD_LIBRARY_PATH=xxx
-- 运行: LD_LIBRARY_PATH=/home/vagrant/.xmake/packages/l/libaio/0.3.113/7966af35fb584a3890e13c065a310e9f/lib/ ./build/linux/x86_64/release/kanglebin -d 3
    add_packages("libaio", "pcre")
    -- 设置链接的库
    add_links("pthread", "dl", "aio", "z", "pcre");

    stdc = "c99"
    set_languages(stdc, "c++11")


    -- cpp和头文件
    -- add_files("./module/access/*.cpp")
    add_includedirs("./module/access/")

    add_files("./module/webdav/*.cpp")
    add_includedirs("./module/webdav/")

    add_files("./module/whm/*.cpp")
    add_includedirs("./module/whm/")

    -- 这个不要
    -- add_files("./extworker/*.cpp")
    -- add_includedirs("./extworker/")

    add_files("./module/whm/whmdso/core/*.cpp")
    add_includedirs("./module/whm/whmdso/core/")

    add_files("./src/*.cpp")
    add_includedirs("./src/")


    -- 递归遍历获取所有子目录
    -- for _, dir in ipairs(os.dirs("lvgl/src/**")) do
    --     add_files(dir.."/*.c");
    --     add_includedirs(dir);
    -- end

    -- -- 递归遍历获取所有子目录
    -- for _, dir in ipairs(os.dirs("lvgl/demos/**")) do
    --     add_files(dir.."/*.c");   -- 添加目录下所有C文件
    --     add_includedirs(dir);  -- 添加目录作为头文件搜索路径
    -- end

    -- for _, v in ipairs(os.dirs("lv_drivers/**")) do
    --     add_files(v.."/*.c");
    --     add_includedirs(v);
    -- end
    add_includedirs(".")
